import app from './src/server';
import { PORT } from './src/constants';

app.listen(PORT, () => console.log(`Server started on port ${PORT} [${process.env.NODE_ENV}]`));
