require('dotenv-flow').config({ default_node_env: 'development' });

import supertest from 'supertest';
import server from '../src/server';

export const request = supertest.agent(server.listen(process.env.PORT));
