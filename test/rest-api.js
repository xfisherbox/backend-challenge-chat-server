/* eslint-disable no-unused-expressions */

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

import WebSocket from 'ws';
import { request } from './common';

var expect = require('chai').expect;

const roomName = Math.random()
    .toString(36)
    .substring(2);

let credentials;
let user_id;
let room_id;

describe('Backend Challenge Chat Server REST API: ', () => {
    before(function(done) {
        request
            .post('/login')
            .send({ user_name: 'KiwiBird', password: '123456' })
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                credentials = 'Basic: ' + res.body.credentials;
                return done();
            });
    });

    describe('GET /users', () => {
        it('should be user list', (done) => {
            request
                .get('/users')
                .set('Authorization', credentials)
                .expect('Content-Type', /json/)

                .expect(200)
                .end((err, res) => {
                    expect(res.body).to.be.a('array');
                    user_id = res.body[0].user_id;
                    done();
                });
        });
    });

    describe('GET /users/:id', () => {
        it('should be one user with request id', (done) => {
            request
                .get('/users/' + user_id)
                .set('Authorization', credentials)
                .expect('Content-Type', /json/)

                .expect(200)
                .end((err, res) => {
                    expect(res.body.user_id).to.be.equal(user_id);

                    done();
                });
        });
    });

    describe('POST /rooms', () => {
        it('should be add a new chat room', (done) => {
            request
                .post('/rooms')
                .send({ name: roomName, user_id })
                .set('Authorization', credentials)
                .expect('Content-Type', /json/)

                .expect(200)
                .end((err, res) => {
                    expect(res.body.room_name).to.be.equal(roomName);
                    room_id = res.body.room_id;
                    if (!room_id) {
                        done('room_id not found!');
                    } else {
                        done();
                    }
                });
        });
    });

    describe('GET /rooms', () => {
        it('should be a room list', (done) => {
            request
                .get('/rooms')
                .set('Authorization', credentials)
                .expect('Content-Type', /json/)

                .expect(200)
                .end((err, res) => {
                    expect(res.body).to.be.a('array');
                    const room = res.body.filter((room) => room.room_id === room_id);
                    if (!room) {
                        done('Room not created');
                    } else {
                        done();
                    }
                });
        });
    });

    describe('GET /rooms/:room_id', () => {
        it('should be a room', (done) => {
            request
                .get('/rooms/' + room_id)
                .set('Authorization', credentials)
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body.room_id).to.be.equal(room_id);
                    done();
                });
        });
    });

    describe('POST /rooms/:room_id/join', () => {
        it('should be join to a room', (done) => {
            request
                .post('/rooms/' + room_id + '/join')
                .send({ user_id })

                .set('Authorization', credentials)
                .expect('Content-Type', /json/)

                .expect(200)
                .end((err, res) => {
                    expect(res.body.result).to.be.equal('user successfully joined the room');
                    done();
                });
        });
    });

    describe('POST /message', () => {
        it('should be a post message and WebSocket connect', (done) => {
            const socket = new WebSocket('wss://localhost:' + process.env.PORT, {
                rejectUnauthorized: false,
            });

            socket.onmessage = function(event) {
                console.log(event.data);

                const message = JSON.parse(event.data);
                expect(message.user_id).to.be.equal(user_id);
                expect(message.room_id).to.be.equal(room_id);
                done();

                socket.close();
            };

            request
                .post('/message')
                .send({ user_id, room_id, message: 'Hello from mocha tests' })
                .set('Authorization', credentials)
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body.result).to.be.equal('message successfully sent');
                    //done();
                });
        });
    });

    describe('POST /message', () => {
        it('should be a post message to chatBot and get reply message', (done) => {
            const socket = new WebSocket('wss://localhost:' + process.env.PORT, {
                rejectUnauthorized: false,
            });

            socket.onmessage = function(event) {
                console.log(event.data);

                const message = JSON.parse(event.data);
                expect(message.room_id).to.be.equal(room_id);
                console.log(message);

                if (!message.message.startsWith('@bot')) {
                    expect(message.user_id).to.be.equal('bot');
                    expect(message.message).to.be.equal('test ok');
                    done();
                    socket.close();
                }
            };

            request
                .post('/message')
                .send({ user_id, room_id, message: '@bot test' })
                .set('Authorization', credentials)
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body.result).to.be.equal('message successfully sent');
                });
        });
    });

    describe('POST /rooms/:room_id/leave', () => {
        it('should be leave from a room', (done) => {
            request
                .post('/rooms/' + room_id + '/leave')
                .send({ user_id })

                .set('Authorization', credentials)
                .expect('Content-Type', /json/)

                .expect(200)
                .end((err, res) => {
                    expect(res.body.result).to.be.equal('user successfully left the room');
                    done();
                });
        });
    });

    describe('DELETE /rooms', () => {
        it('should be delete a room', (done) => {
            request
                .delete('/rooms/' + room_id)
                .send({ user_id })

                .set('Authorization', credentials)
                .expect('Content-Type', /json/)

                .expect(200)
                .end((err, res) => {
                    expect(res.body.result).to.be.equal('room removed successfully');
                    done();
                });
        });
    });
});
