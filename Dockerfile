FROM node:11.9.0-alpine
ENV NODE_ENV production
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
RUN apk --no-cache add --virtual native-deps \
    g++ gcc libgcc libstdc++ linux-headers make python && \
    npm install --quiet node-gyp -g &&\
    npm install --quiet && \
    npm install nodemon && \
    apk del native-deps
RUN apk add screen
RUN npm install --quiet && npm install -g mocha
COPY . .
