# Backend Challenge Chat Server

Backend Challenge Chat Server application

### Environment

-   [Node.js](https://nodejs.org/en/)
-   [Npm](https://www.npmjs.com)
-   [MongoDB](https://www.mongodb.com/)
-   [Dialogflow](http://dialogflow.com)
-   [Docker](https://www.docker.com)
-   [Ngrok](https://ngrok.com)

### Main Node Libraries

-   [Koa.js](https://koajs.com/) - Web framework
-   [Passport](http://www.passportjs.org/) - Authorization strategories
-   [Mongoose](https://mongoosejs.com) - Mongo object modeling
-   [Dialogflow](http://dialogflow.com) - Dialogflow is an enterprise-grade NLU platform that makes it easy for developers to design and integrate conversational user interfaces into mobile apps, web applications, devices, and bots.

### Installation

-   git clone https://gitlab.com/xfisherbox/backend-challenge-chat-server
-   cd backend-challenge-chat-server
-   docker-compose -f "docker-compose.yml" up -d --build
-   curl https://localhost/login --insecure -H Content-type:application/json -d '{ "user_name": "KiwiBird", "password": "123456"}'

### Descriptions

-   API REST port - 443
-   WebSocket Port - 1030

### Test

-   git clone https://gitlab.com/xfisherbox/backend-challenge-chat-server
-   cd backend-challenge-chat-server
-   npm i
-   npm test
