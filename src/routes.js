import compose from 'koa-compose';
import Router from 'koa-router';

import AuthRouter from './modules/auth/auth.routes';
import UserRouter from './modules/user/user.routes';
import RoomRouter from './modules/room/room.routes';
import MessageRouter from './modules/message/message.routes';
import OrderRouter from './modules/order/order.routes';
import ChatbotRouter from './modules/chatbot/chatbot.routes';

const router = new Router();

router.get('/', async (ctx) => {
    ctx.body = 'Chat Server API';
});

router.use('/login', AuthRouter.routes(), AuthRouter.allowedMethods());
router.use('/users', UserRouter.routes(), UserRouter.allowedMethods());
router.use('/rooms', RoomRouter.routes(), RoomRouter.allowedMethods());
router.use('/message', MessageRouter.routes(), MessageRouter.allowedMethods());
router.use('/order', OrderRouter.routes(), OrderRouter.allowedMethods());
router.use('/chatbot', ChatbotRouter.routes(), ChatbotRouter.allowedMethods());

router.get('*', async (ctx) => {
    ctx.body = { status: 404 };
});

export default function routes() {
    return compose([router.routes(), router.allowedMethods()]);
}
