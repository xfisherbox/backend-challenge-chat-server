import Koa from 'koa';
import fs from 'fs';
import bodyParser from 'koa-bodyparser';
import logger from 'koa-logger';
import passport from 'koa-passport';
import session from 'koa-session';
import cors from '@koa/cors';
import websockify from 'koa-wss';
import mongoose from 'mongoose';
import https from 'https';
import uuid from 'uuid';
import cache from 'memory-cache';
import users from './models/user';
import rooms from './models/room';
import orders from './models/order';
import WebhookController from './modules/webhook/webhook.controller';
import messages from './models/message';
import './modules/auth/strategies';
import routes from './routes';
import { MONGO_URI } from './constants';

mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.connect(MONGO_URI, { useNewUrlParser: true, autoIndex: process.env.NODE_ENV !== 'production' });
mongoose.connection.on('error', console.error);

const httpsOptions = {
    key: fs.readFileSync('keys/server.key'),
    cert: fs.readFileSync('keys/server.crt'),
};

const server = https.createServer(httpsOptions);
//const app = websockify(new Koa(), { server }, httpsOptions);
const app = websockify(new Koa());

app.db = { users, rooms, messages, orders };
app.cache = cache;
app.selectIntent = new WebhookController();

(async (users) => {
    const chatBot = await users.findOne({ user_name: '@bot' });
    if (!chatBot) app.chatBot = await users.create({ user_name: '@bot', password: uuid.v4() });
    else app.chatBot = chatBot;
})(users);

app.use(
    session(
        {
            domain: process.env.SESSION_DOMAIN,
        },
        app
    )
);

app.use(
    cors({
        origin: process.env.CORS_ALLOWED_ORIGIN,
        credentials: true,
    })
);

app.use(logger());
app.use(bodyParser());

app.use(passport.initialize());
app.use(passport.session());

app.use(routes());

export default app;
