import _ from 'lodash';

export default class ChatbotController {
    async create(ctx) {
        const { queryResult, session } = ctx.request.body;

        const { intent } = queryResult;

        console.log(intent.name, session);

        const message = await ctx.app.selectIntent.select(intent.name, _.get(queryResult, 'parameters'));

        ctx.body = {
            fulfillmentText: message,
        };
    }
}
