import Router from 'koa-router';
import Controller from './auth.controller';

const router = new Router();
const controller = new Controller();

router.post('/', async (ctx) => {
    await controller.login(ctx);
});

export default router;
