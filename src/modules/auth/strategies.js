import passport from 'koa-passport';
import { Strategy as JWTStrategy, ExtractJwt } from 'passport-jwt';
import { SECURITY_JWTSECRET_SECRET } from '../../constants';

import users from '../../models/user';

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('Basic:'),
    secretOrKey: SECURITY_JWTSECRET_SECRET,
};

passport.use(
    new JWTStrategy(jwtOptions, async (payload, done) => {
        const user = await users.findOne({ user_id: payload.user_id });

        if (user) {
            done(null, user);
        } else {
            done(null, false);
        }
    })
);
