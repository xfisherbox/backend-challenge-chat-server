import jwt from 'jsonwebtoken';

import { SECURITY_JWTSECRET_SECRET } from '../../constants';

export default class AuthController {
    async login(ctx) {
        const { user_name, password } = ctx.request.body;
        const { users } = ctx.app.db;

        if (!user_name || !password) {
            ctx.throw(400, 'Missing username and/or password parameter');
        }

        let user = await users.findOne({ user_name });

        if (!user) {
            user = await users.create({ user_name, password });
        }

        const payload = { user_id: user.user_id, user_name: user.user_name };

        const token = jwt.sign(payload, SECURITY_JWTSECRET_SECRET);

        ctx.body = {
            user_id: user._id,
            user_name: user.user_name,
            credentials: token,
        };
    }

    async users(ctx) {
        const { users } = ctx.app.db;

        ctx.body = await users
            .find({}, { user_name: 1, _id: 1 })
            .map((user) => ({ user_id: _id, user_name: user.user_name }));
    }
}
