export default class UserController {
    async users(ctx) {
        const { users } = ctx.app.db;

        ctx.body = (await users.find({}, { user_name: 1 })).map((user) => ({
            user_id: user._id,
            user_name: user.user_name,
        }));
    }

    async user(ctx) {
        const { users } = ctx.app.db;
        const { id } = ctx.params;

        const user = await users.findOne({ _id: id }, { user_name: 1 });

        ctx.body = { user_id: user._id, user_name: user.user_name };
    }
}
