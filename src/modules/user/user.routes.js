import { authMiddleware } from '../../common';

import Router from 'koa-router';
import Controller from './user.controller';

const router = new Router();
const controller = new Controller();

router.get('/', authMiddleware, async (ctx) => {
    await controller.users(ctx);
});

router.get('/:id', authMiddleware, async (ctx) => {
    await controller.user(ctx);
});

export default router;
