import uuid from 'uuid/v1';

export default class RoomController {
    async join(ctx) {
        const { id } = ctx.params;
        const { rooms, users } = ctx.app.db;
        const { user_id } = ctx.request.body;

        if (!id) {
            ctx.throw(400, 'Missing room_id!');
        }

        if (!user_id) {
            ctx.throw(400, 'Missing user_id!');
        }

        const user = await users.findOne({ _id: user_id });

        if (!user) {
            ctx.throw(400, 'User not found!');
        }
        const room = await rooms.findOneAndUpdate(
            { _id: id, users: { $ne: user._id } },
            { $addToSet: { users: user._id }, new: true }
        );

        if (!room) {
            ctx.throw(400, 'You are already in this room!');
        }

        ctx.body = {
            result: 'user successfully joined the room',
        };
    }

    async leave(ctx) {
        const { id } = ctx.params;
        const { rooms, users } = ctx.app.db;

        const { user_id } = ctx.request.body;

        if (!id) {
            ctx.throw(400, 'Missing room_id!');
        }

        const user = await users.findOne({ _id: user_id });

        const room = await rooms.findOneAndUpdate(
            { _id: id, users: { $eq: user._id } },
            { $pull: { users: user._id }, new: true }
        );

        if (!room) {
            ctx.throw(400, 'You are not in this room!');
        }

        ctx.body = {
            result: 'user successfully left the room',
        };
    }

    async get(ctx) {
        const { id } = ctx.params;
        const { rooms } = ctx.app.db;

        if (!id) {
            ctx.throw(400, 'Missing room_id!');
        }
        const room = await rooms
            .findOne(
                { _id: id },
                {
                    room_name: 1,
                    creator_id: 1,
                    createdAt: 1,
                    users: 1,
                }
            )
            .populate('creator_id')
            .populate('users');

        if (!room) {
            ctx.throw(400, 'Room not found!');
        }
        ctx.body = {
            room_id: room._id,
            room_name: room.room_name,
            creator_id: room.creator_id.user_id,
            created_at: room.createdAt,
            users: room.users.map((user) => user.user_id),
        };
    }

    async list(ctx) {
        const { rooms } = ctx.app.db;

        const roomList = await rooms
            .find(
                {},
                {
                    room_name: 1,
                    creator_id: 1,
                    createdAt: 1,
                    users: 1,
                }
            )
            .populate('creator_id')
            .populate('users');
        const normalize = roomList.map((room) => ({
            room_id: room._id,
            room_name: room.room_name,
            creator_id: room.creator_id.user_id,
            created_at: room.createdAt,
            users: room.users.map((user) => user._id),
        }));

        ctx.body = normalize;
    }
    async create(ctx) {
        const { rooms, users } = ctx.app.db;
        const { name, user_id } = ctx.request.body;

        if (!name || !user_id) {
            ctx.throw(400, 'Missing name and/or user_id parameter');
        }

        const user = await users.findOne({ _id: user_id });

        if (!user) {
            ctx.throw(400, 'Invalid user_id. User not found!');
        }

        try {
            const room = await rooms.create({ room_name: name, creator_id: user._id });
            ctx.body = {
                room_id: room._id,
                room_name: room.room_name,
                creator_id: room.creator_id,
                created_at: room.createdAt,
            };
        } catch (err) {
            console.log(err);
            if (err.code === 11000) {
                ctx.throw(400, 'This room name already exists!!');
            }
            ctx.throw(400, err);
        }
    }

    async remove(ctx) {
        const { id } = ctx.params;
        const { user_id } = ctx.request.body;
        const { rooms, users } = ctx.app.db;

        const user = await users.findOne({ _id: user_id });

        if (!user) {
            ctx.throw(400, 'Invalid user_id. User not found!');
        }

        const room = await rooms.findOne({ _id: id, creator_id: user._id });

        if (!room) {
            ctx.throw(400, 'Invalid room_id or user_id. Room not found!');
        }

        if (!room.creator_id.equals(user._id)) {
            ctx.throw(400, 'You cannot remove this room!');
        }

        await rooms.deleteOne({ room_id: id, creator_id: user._id });

        ctx.body = { result: 'room removed successfully' };
    }
}
