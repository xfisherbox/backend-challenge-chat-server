import { authMiddleware } from '../../common';

import Router from 'koa-router';
import Controller from './room.controller';

const router = new Router();
const controller = new Controller();

router.post('/', authMiddleware, async (ctx) => {
    await controller.create(ctx);
});

router.delete('/:id', authMiddleware, async (ctx) => {
    await controller.remove(ctx);
});

router.get('/', authMiddleware, async (ctx) => {
    await controller.list(ctx);
});

router.post('/:id/join', authMiddleware, async (ctx) => {
    await controller.join(ctx);
});

router.get('/:id', authMiddleware, async (ctx) => {
    await controller.get(ctx);
});

router.post('/:id/leave', authMiddleware, async (ctx) => {
    await controller.leave(ctx);
});

export default router;
