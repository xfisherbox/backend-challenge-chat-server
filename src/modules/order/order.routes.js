import { authMiddleware } from '../../common';

import Router from 'koa-router';
import Controller from './order.controller';

const router = new Router();
const controller = new Controller();

router.post('/', async (ctx) => {
    await controller.create(ctx);
});

export default router;
