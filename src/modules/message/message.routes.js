import Router from 'koa-router';
import Controller from './message.controller';
import { authMiddleware } from '../../common';

const router = new Router();
const controller = new Controller();

router.post('/', authMiddleware, async (ctx) => {
    await controller.create(ctx);
});

export default router;
