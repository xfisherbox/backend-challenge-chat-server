import dialogflow from 'dialogflow';
import _ from 'lodash';
import moment from 'moment';
const { struct } = require('pb-util');

import { CHATBOT_DEFAULT_LANGUAGE, CHATBOT_CMD_PREFFIX, CHATBOT_DIALOGFLOW_PROJECT_ID } from '../../constants';

export default class MessageController {
    async sendToAllWSClients({ ctx, user_id, room_id, message }) {
        ((data) => {
            ctx.app.ws.server.clients.forEach((client) => {
                client.send(data);
            });
        })(JSON.stringify({ user_id, room_id, message }));
    }
    async setLanguage({ cache, lang, room_id, sessionPath, sessionClient }) {
        if (lang) {
            if (lang.toLowerCase().startsWith('ukr')) {
                await cache.put(room_id, {
                    sessionPath,
                    sessionClient,
                    languageCode: 'uk',
                });
            } else if (lang.toLowerCase().startsWith('анг')) {
                await cache.put(room_id, {
                    sessionPath,
                    sessionClient,
                    languageCode: 'en-US',
                });
            }
        }
    }
    async createBotSession({ room_id, cache }) {
        const sessionClient = new dialogflow.SessionsClient();

        const sessionPath = sessionClient.sessionPath(CHATBOT_DIALOGFLOW_PROJECT_ID, room_id);

        const session = {
            sessionPath,
            sessionClient,
            languageCode: CHATBOT_DEFAULT_LANGUAGE,
        };
        await cache.put(room_id, session);

        return session;
    }

    async setBotParameters({ ctx, sessionPath }) {
        const { messages, rooms } = ctx.app.db;

        const contextClient = new dialogflow.ContextsClient();

        const totalRooms = await rooms.countDocuments({});
        const totalMessages = await messages.countDocuments({});

        const contextData = {
            name: contextClient.contextPath(
                CHATBOT_DIALOGFLOW_PROJECT_ID,
                sessionPath.split('/').slice(-1)[0],
                'server'
            ),
            parameters: struct.encode({ totalRooms, totalMessages }),
            lifespanCount: 1,
        };

        await contextClient.createContext({
            parent: sessionPath,
            context: contextData,
        });
    }
    async requestToBot({ sessionPath, sessionClient, text, languageCode }) {
        const request = {
            session: sessionPath,
            queryInput: {
                text: {
                    text,
                    languageCode,
                },
            },
        };

        const responses = await sessionClient.detectIntent(request);

        const result = responses[0].queryResult;

        return result;
    }

    async chatBotRequest({ ctx, trimedMessage, room }) {
        const { cache, chatBot } = ctx.app;
        const { messages, orders } = ctx.app.db;
        const { room_id, user_id } = ctx.request.body;

        let botSession = await cache.get(room_id);

        if (!botSession) {
            botSession = await this.createBotSession({ room_id, cache });
        }

        const { sessionPath, sessionClient, languageCode } = botSession;

        await this.setBotParameters({ ctx, sessionPath });

        const result = await this.requestToBot({
            sessionPath,
            sessionClient,
            text: trimedMessage.slice(CHATBOT_CMD_PREFFIX.length),
            languageCode,
        });

        const message = result.fulfillmentText;

        const lang = _.get(result, 'parameters.fields.language.stringValue', null);

        if (lang) await this.setLanguage({ cache, lang, room_id, sessionPath, sessionClient });

        await messages.create({
            user_id: chatBot._id,
            room_id: room._id,
            message,
        });

        this.sendToAllWSClients({ ctx, user_id: 'bot', room_id, message });
    }

    async create(ctx) {
        const { rooms, users, messages } = ctx.app.db;
        const { room_id, user_id, message } = ctx.request.body;

        if (!room_id || !user_id || !message) {
            ctx.throw(400, 'Missing room_id, user_id and/or message parameter!');
        }

        const user = await users.findOne({ _id: user_id });

        if (!user) {
            ctx.throw(400, 'Invalid user_id. User not found!');
        }

        const room = await rooms.findOne({ _id: room_id, users: { $eq: user._id } });

        if (!room) {
            ctx.throw(400, 'Invalid room_id. Room not found or you are not in room!');
        }

        await messages.create({ user_id: user._id, room_id: room._id, message });

        this.sendToAllWSClients({ ctx, user_id, room_id, message });

        const trimedMessage = message.trim();

        if (trimedMessage.startsWith(CHATBOT_CMD_PREFFIX)) {
            this.chatBotRequest({
                ctx,
                trimedMessage,
                user_id,
                room,
            });
        }

        ctx.body = {
            result: 'message successfully sent',
        };
    }
}
