import _ from 'lodash';
import moment from 'moment';
import orders from '../../models/order';

const intents = {
    showOrder: '1e09f6de-7b2b-4c19-8414-3fe3139c61ca',
    removeOrder: 'dbe259db-8aee-43d5-b45e-91ec9d87ad1e',
    removeAllOrder: 'd426810f-37ec-4e7d-ab6a-63a4cdd68125',
    createOrder: '4639657f-a94d-4385-b0ab-f363af66e0f7',
    changeOrderTime: '699dc1f2-a80e-4c80-ba74-71673fc83cf7',
};

export default class WebhookController {
    async select(name, params) {
        console.log(params);
        const intentId = name.split('/').pop();

        const action = _.findKey(intents, (intent) => intent === intentId);

        switch (action) {
            case 'showOrder':
                return await this.showOrders();
            case 'removeOrder':
                return await this.removeOrder(params.orderId);
            case 'removeAllOrder':
                return await this.removeAllOrder();
            case 'createOrder':
                return await this.createOrder(params);
            case 'changeOrderTime':
                return await this.changeOrderTime(params);
            default:
                console.log('no switch');
        }

        return 'No action event';
    }

    async changeOrderTime(params) {
        const datetime = params.time;
        const order = await orders.updateOne({ _id: params.orderId }, { order_date: datetime });

        if (order) {
            return 'Update sucessfully!';
        } else {
            return 'Order not found!';
        }
    }

    async createOrder(params) {
        const { date, time, pizzas } = params;

        const mdate = moment(date);
        const mtime = moment(time);

        const datetime = moment([mdate.year(), mdate.month(), mdate.date(), mtime.hour(), mtime.minute()]).utc();

        const order = await orders.create({
            order_date: datetime,
            pizzas,
            //room_id,
            //user_id,
        });
        return `Order created with id ${order._id}`;
    }

    async removeAllOrder() {
        await orders.deleteMany({});
        return 'All your orders successfully removed!';
    }

    async removeOrder(id) {
        const order = await orders.deleteOne({ _id: id });
        if (order) {
            return 'Order successfully removed!';
        } else {
            return 'Order not found!';
        }
    }
    async showOrders() {
        const myOrders = await orders.find({}, { _id: 1, status: 1, pizzas: 1, order_date: 1 });

        const normalize = myOrders.map((order) => {
            return { order_date: moment(order.order_date), ...order.toObject() };
        });

        let strOrders = JSON.stringify(normalize, null, ' ');

        if (myOrders.length === 0) strOrders = 'No orders!';

        return strOrders;
    }
}
