import mongoose from 'mongoose';

const { Schema } = mongoose;
const { String, ObjectId } = mongoose.Schema.Types;

const messageSchema = new Schema(
    {
        user_id: { type: ObjectId, ref: 'User' },
        room_id: { type: ObjectId, ref: 'Room' },
        status: { type: String, enum: ['created', 'canceled'], default: 'created' },
        order_date: { type: Date },
        pizzas: [String],
    },
    {
        timestamps: true,
    }
);

export default mongoose.model('Order', messageSchema);
