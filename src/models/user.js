import mongoose from 'mongoose';
import sha256 from 'sha256';

const { Schema } = mongoose;
const { String } = mongoose.Schema.Types;

const userSchema = new Schema(
    {
        user_name: { type: String, index: true, unique: true, required: true },
        password: { type: String, required: true, set: (password) => sha256(password) },
    },
    {
        timestamps: true,
    }
);

userSchema.index({ user_name: 1 }, { unique: true });

export default mongoose.model('User', userSchema);
