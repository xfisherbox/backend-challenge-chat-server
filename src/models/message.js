import mongoose from 'mongoose';

const { Schema } = mongoose;
const { String, ObjectId } = mongoose.Schema.Types;

const messageSchema = new Schema(
    {
        user_id: { type: ObjectId, ref: 'User' },
        room_id: { type: ObjectId, ref: 'Room' },
        message: { type: String, required: true },
    },
    {
        timestamps: true,
    }
);

export default mongoose.model('Message', messageSchema);
