import mongoose from 'mongoose';
import uuid from 'uuid/v1';

const { Schema } = mongoose;
const { String, ObjectId } = mongoose.Schema.Types;

const roomSchema = new Schema(
    {
        room_name: { type: String, index: true, unique: true, required: true },
        creator_id: { type: ObjectId, ref: 'User' },
        users: [{ type: ObjectId, ref: 'User' }],
    },
    {
        timestamps: true,
    }
);

roomSchema.index({ room_name: 1 }, { unique: true });

export default mongoose.model('Room', roomSchema);
