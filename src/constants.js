export const PORT = process.env.PORT || 1030;

export const MONGO_URI = process.env.MONGO_URI || 'mongodb://localhost:27017/challenge-chat-server';

export const SECURITY_JWTSECRET_SECRET =
    process.env.SECURITY_JWTSECRET_SECRET || '927346r78f6re876fffFer87638476346374##$4';

export const CHATBOT_DEFAULT_LANGUAGE = 'en-US';
export const CHATBOT_CMD_PREFFIX = '@bot ';
export const CHATBOT_DIALOGFLOW_PROJECT_ID = 'challengebot-tkhvaa';
