import passport from 'passport';

export const authMiddleware = async (ctx, next) => {
    if (ctx.isAuthenticated()) {
        return next();
    }

    const { authorization } = ctx.request.header;
    if (authorization) {
        return passport.authenticate('jwt', { session: false }, async (err, user) => {
            if (err) ctx.throw(500);

            if (!user) {
                ctx.throw(401);
            }
            ctx.state.user = user;

            return next();
        })(ctx, next);
    }

    return ctx.throw(401);
};
